\section{INTRODUCTION}
\label{intro}

Modern applications demand greater main memory capacity for fulfilling user
needs. Unfortunately, DRAM scaling has
slowed~\cite{Mutlu:204:DRAM:Scaling,Lim:2009}, leading researchers to explore
alternative technologies.  Non-Volatile Memory (NVM) exhibits promising
features: byte addressability, high density, scalability (capacity), negligible
idle power, and non-volatility~\cite{after-hard-drives}. A promising technology
is Phase Change Memory (PCM), but it has shortcomings: high access latency,
write latency exceeds read latency, high write energy, and low write
endurance~\cite{lee-pcm-isca,moin-pcm-isca}.  

Improvements in manufacturing technology are bridging the latency
gap~\cite{ITRS-PCM-LATENCY}.  Finite write endurance is however more
challenging because each write changes the material
form~\cite{PCM-Vacum-Science}.  To make NVM practical, systems need to exploit
NVM's capacity without compromising its lifetime.  Prior work proposes
\emph{hybrid memory} that combines DRAM and PCM~\cite{lee-pcm-isca,
moin-pcm-isca} to achieve the best of both approaches: low latency, high
capacity, energy efficiency and durability.  Prior hardware and OS solutions
mitigate writes to NVM by reactively placing highly mutated pages in
DRAM~\cite{lee-pcm-isca, moin-pcm-isca, ricardo-os,sally-semantics-hybrid}.
These solutions assume existing programming models, although hybrid memories
can support a mix of existing and new programming models, including using PCM
as a disk replacement and persistent heap data
structures~\cite{condit:2009,Swanson:2013}. This paper also focuses on
exploiting hybrid memories with existing programming models and thus requires
no additional programming effort. 

We propose a runtime that automatically manages allocation of objects in DRAM
and NVM.  We exploit the memory manager (garbage collector) in managed runtimes
for languages such as Java, C\#, JavaScript, and Python.  Our goal is to place
as much of the program's dynamic heap in NVM as possible to exploit NVM
capacity, while minimizing writes to NVM to prevent wear out.  Our hypothesis
is that most objects are not highly written and thus the goal of this proposed
division is possible.  An empirical analysis of write behaviors in Java
applications motivates our approach.  We find that, on average, 70\% of the
writes in Java applications occur to nursery objects, motivating a design that
allocates them to DRAM. Of the writes to mature objects, 2\% of mature objects
sustain 80\% of these writes.  In this paper, we propose a profiling mechanism
to predict mature object behaviors and a garbage collector that promotes mature
objects to DRAM or NVM based on its predictions.

\begin{figure}[tb!]
	\centering
	\includegraphics[width= 8cm]{figures/heap.pdf}
	\vspace{-2mm}
	\caption{
                 Heap organization for hybrid memories. \emph{Highly mutated nursery survivors are
                 promoted to DRAM during a garbage collection. The mutator allocates highly mutated large objects in DRAM.} 
        }
	\label{fig:heap}
	\vspace{-3mm}
\end{figure}

Our approach exposes the physical memory organization to the runtime.
Figure~\ref{fig:heap} shows how our proposed heap organization maps on to
hybrid DRAM and PCM memories. We modify the JVM to explicitly request DRAM and
NVM from the OS.  The mutator allocates new objects in a DRAM nursery, because
the nursery is highly mutated. During a garbage collection (GC), nursery survivors are promoted to either DRAM or
NVM. Large objects that do not fit in the nursery are allocated directly in the
mature space.  We use write-intensity prediction to promote mature objects and
allocate large objects into DRAM or NVM.  The predictor identifies
write-intensive objects, which on average for our applications, constitute less
than 10\% of all mature objects. 

We measure writes to objects in 12 Java applications, and find that objects
originating from the same allocation site in a program show uniform write
behavior.  Most allocation sites are dominated by either write-intensive
objects or rarely written objects. We classify sites as write-intensive (DRAM)
or not (NVM). During production execution, the garbage collector exploits this
classification for allocating large objects, and promoting nursery survivors to
either DRAM or NVM.  We also try other prediction mechanisms, such as object
size and class-type, but they are less accurate.  

We evaluate two heuristics. Both require allocation-site homogeneity. A site is
classified as DRAM if a fraction of objects allocated from it are write
intensive.  To identify write-intensive objects, \writefreq (\first) counts the
number of writes to individual objects.  Objects that get more than a threshold
of writes are write intensive. \first eliminates 90\% of the writes to the NVM
mature space by placing 6\% of the mature heap in DRAM.  \emph{\writeden}
(\second) computes write-intensity by dividing writes to an object by object
size in bytes. Objects with density more than a cutoff density are write
intensive. \second eliminates 90\% of the writes to NVM mature space and places
1\% of the mature heap in DRAM. By taking into account both object size and
writes, \second thus results in a higher NVM utilization.

In summary, the contributions of this paper are:

\begin{itemize}

\item demonstrating that writes to mature objects in Java are
predictable on a per allocation-site basis;

\item using profiling information to predict sites as write-intensive
  (DRAM) or not (NVM);

\item a garbage collector that uses write-intensity prediction to
  allocate objects in DRAM and NVM; and 

\item results that show this approach can limit writes to NVM while
  still exploiting the capacity advantage of NVM.
\end{itemize}

This paper leaves as an open question performance of hybrid memories, and
questions such as if highly read objects will also need to reside in DRAM to
meet application latency requirements. As memory systems evolve to meet
application needs, we believe the hardware, operating system, and language
runtime implementations will all have a role to play.

