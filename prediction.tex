\vspace*{-0.5em}
\section{WRITE-INTENSITY PREDICTION}
\label{sec:predict}

This section presents our write homogeneity metric and prediction
mechanisms. We then describe how the collector exploits this
information to allocate objects
in hybrid memories. % DRAM and NVM memory systems.

\vspace*{-0.5em}
\subsection{Write Homogeneity}

\noindent\textbf{Computing write homogeneity.} To discover if allocation-time information
will accurately predict the write-intensive objects, we first measure 
write homogeneity in our applications.  We analyze the write-intensity traces (see
Section~\ref{sec:methodology}) by grouping together objects based on
type, size, and site at allocation time. When analyzing object type,
size and site,
we first identify all the
unique types, sizes, and allocations (metrics). We then measure the distribution of write-intensive versus
other objects for each metric. Using these distributions, we
compute the entropy of each metric.  The entropy of a type is defined as as
follows:


\vspace*{-1.5em}
\begin{equation} \label{eq1}
\begin{split} E = -\big(O_w \times {\log_2 O_w}\big) - \big(O_{nw} \times {\log_2 O_{nw}}\big) \\ \end{split}
\end{equation}

\begin{figure}[bt]
	\centering
	\subfloat[Write-Frequency Threshold = 1K] {\includegraphics[width=8cm]{./figures/H1.pdf}}

	\centering
	\subfloat[Cutoff Density = 1] {\includegraphics[width=8cm]{./figures/H2.pdf}}

	\vspace*{-1em}
	\caption{Homogeneity of object writes on a per size, class-type, and allocation-site basis. \emph{The homogeneity of 
                 allocation-site is high: 90\% of the heap volume originates from sites that have ~90\% of the same object kind.}}
	\vspace*{-1em}
	\label{fig:HCurve}
\end{figure}

$O_w$ is the fraction of write-intensive objects and $O_{nw}$ is the fraction
of other objects.  \kathryn{What does low entropy of 0 vs high entropy
  of 1 mean in this context?}
Similar to types, we compute the entropy of
all sizes and allocation sites. We use two criteria to identify write-intensive objects:
(1) \emph{write-frequency:} the object gets more than a threshold of number of writes,
and (2) \emph{write-density:} the object has more than a threshold writes per byte.

\kathryn{We have not adequately described the intuitive meaning of the
  x-axis on the bottom and the one on the top of these figures.}

\noindent\textbf{Results.} Figure~\ref{fig:HCurve} shows homogeneity curves
averaged over all benchmarks.  We use a write-intensity threshold ($w_t$) of 1K
in Figure~\ref{fig:HCurve} (a).  Figure~\ref{fig:HCurve} (b) uses a density
threshold ($d_{cut}$) of one.  For each entropy value, we calculate the total
volume of mature object-sizes, types, and sites whose entropy is less than or
equal to that value.  An entropy of zero means uniformity, i.e., all mature
objects allocated by the site exhibit the same write behavior. An entropy of
one means the site is difficult to predict because objects have a range of
write characteristics. 

\ignore{A site with an entropy of one allocates 50\% objects that
are write-intensive.} \kathryn{These last two statements seem to be in conflict?}

\kathryn{Please check that my edits below are correct.}
Consider Figure~\ref{fig:HCurve} (a) with the results for a $w_t$ of
1K. 100\% homogeneity would mean that for that metric, all mature
object volume allocated by that site are either write-intensive or not.
We observe that 30\% of the heap
volume are objects with sizes that exhibit uniform
write behavior. Using types, 50\% of the heap volume has
uniform write behavior.  Site is a better predictor compared to both size and
type --- 85\% of the mature heap volume
originates from sites that are 100\% uniform. Surprisingly, size exhibits
better write homogeneity than type.  For 98.7\% homogeneity, both size and type
are good predictors. However, site is preferable to size for predicting
write intensity because site divides heap memory into more
finer-granularity groups. Table~\ref{tab:table-pass} shows the number of unique
sizes, types and sites for the mature objects in our benchmarks. Note that the number of bytes per site is lower
compared to bytes per size. 

From Figure~\ref{fig:HCurve} (a), we conclude that allocation site is a better
predictor of which mature objects get at least 1K writes. We try different
write-intensity thresholds and observe similar behavior.  As the entropy
increases (and homogeneity decreases), the heap volume increases sharply until
a point, after which it starts to flatten out in all three curves.  More than
90\% of the mature heap volume originates from allocation-sites that have 90\%
of the objects of one kind, write-intensive or non-write-intensive. 

Figure~\ref{fig:HCurve} (b) shows write density is a better predictor
than write frequency.  The site homogeneity with a $d_{cut}$ of one is
high. Close to 90\% of the mature heap volume has
100\% of objects either write intensive or not. Size and type are not
as predictive.  The percentage of homogeneous objects is 40\% when considering
size, and 50\% when considering types. 



\vspace*{-0.7em}
\subsection{Allocation-Site Classification}
Although our system promotes mature objects from DRAM to either DRAM
or NVM during nursery collections, the system identifies, at
allocation time, objects as destined for DRAM or NVM promotions.

We propose two heuristics to classify allocation sites as DRAM or NVM.  Both
use the homogeneity threshold ($h_t$) for classifying sites.  If the fraction of
write-intensive objects allocated from a site is above the homogeneity
threshold, then the site is classified as DRAM, otherwise the site is
classified as NVM. The heuristics differ in their criteria to identify
write-intensive objects.

\noindent\textbf{\writefreq (\nospacefirst)} \first uses the frequency of writes to objects
to identify write-intensive objects. If an object gets more than a
threshold of writes (write-frequency threshold or $w_f$), \first considers the
object as write intensive.

\noindent\textbf{\writeden (\nospacesecond)} A drawback of \first is that it does not take
the size of an object into account. Our purpose is to exploit NVM's capacity
and minimize writes to it. We also explore write-density for classifying sites.
Write-density of an object is the ratio of writes to the size of an object in
bytes.  We classify objects with density greater than a threshold 
($d_{cut}$) as write-intensive.


\vspace*{-0.8em}
\section{PREDICTION-GUIDED GARBAGE COLLECTION}

Figure~\ref{fig:heap} shows the basic organization of our approach.
Large objects go directly to a non-moving space in DRAM or NVM based
on a prediction. The nursery for all other initial object allocations
resides in DRAM. Nursery collections promote objects into either DRAM
or NVM based on a prediction.

We store the identifiers of the sites classified as write-intensive
(DRAM) in an advice file.  The advice file is materialized in the
runtime system as a hash table indexed by allocation site number. The
hash table only contains the write-intensive allocation sites for
DRAM.  The allocator and collector use this advice for placing objects
in hybrid memory.

\noindent\textbf{Allocation.}  When the mutator allocates objects in the DRAM
nursery space, it uses the allocation site identifier to query the hash table.
If the allocation site is in the hash table, it marks a bit in the object's
header, which indicates that should the collector promotes this object in the
future,  it should copy it to DRAM.  

For large object allocations, the mutator queries the hash table and then
directly allocates the large object in either DRAM or NVM, as indicated.

\noindent\textbf{Minor collections.} During a nursery collection, as the garbage
collector traces the live objects, it checks their write-intensity header bit.
If the bit is set, the collector copies the object to the mature DRAM space.
Otherwise, it allocates the object in NVM.

\noindent\textbf{Major collections.} During a major collection, the garbage
collector reclaims dead memory in the DRAM and NVM portions of the mature
space. Our system  does not relocate mature objects during a major collection
because relocation incurs writes. The generous capacity of NVM makes
fragmentation less of a concern, but  balancing fragmentation and writes could
be an area for future work.
